package fr.shyndard.over2craft.infectedv2.method;


public enum GameStatus {
	
  WAITING(0, "Waiting"),  
  LAUNCHING(1, "Launching"),  
  INGAME(2, "InGame"),  
  END(3, "End"),
  STOP(3, "Stop");
  
  Integer status;
  String name;
  
  private GameStatus(Integer value, String name) {
    this.status = value;
    this.name = name;
  }
  
  public Integer getValue() {
    return this.status;
  }
  
  public String getName() {
    return this.name;
  }
  
  public static GameStatus getStatus(Integer value) {
    for (GameStatus status : GameStatus.values()) {
      if (status.getValue() == value) {
        return status;
      }
    }
    return WAITING;
  }
}
