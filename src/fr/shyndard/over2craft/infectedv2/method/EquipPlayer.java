package fr.shyndard.over2craft.infectedv2.method;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_10_R1.inventory.CraftItemStack;
import org.bukkit.enchantments.EnchantmentWrapper;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import fr.shyndard.over2craft.api.APIGlobalFunction;
import fr.shyndard.over2craft.api.player.APIPlayer;
import fr.shyndard.over2craft.infectedv2.Main;
import net.minecraft.server.v1_10_R1.NBTTagCompound;
import net.minecraft.server.v1_10_R1.NBTTagInt;
import net.minecraft.server.v1_10_R1.NBTTagList;
import net.minecraft.server.v1_10_R1.NBTTagString;

public class EquipPlayer {
	
	public static void set(Player player, Integer role) {
		APIGlobalFunction.clear(player);
		if(role == 0) setHumanEquipment(player);
		else if(role == 1) setZombieEquipment(player);
	}
	
	private static void setZombieEquipment(Player player) {
		ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (short)2);
		ItemStack chestplate = new ItemStack(Material.matchMaterial("299"), 1);
		    LeatherArmorMeta PlastronMeta = (LeatherArmorMeta)chestplate.getItemMeta();
		    PlastronMeta.setColor(Color.GREEN);
		    PlastronMeta.spigot().setUnbreakable(true);
		    if(APIPlayer.getPlayer(player).hasPermission("infected.gamer")) PlastronMeta.addEnchant(new EnchantmentWrapper(0), 1, true);
		    chestplate.setItemMeta(PlastronMeta);
	    ItemStack leggins = new ItemStack(Material.matchMaterial("300"), 1);
		    LeatherArmorMeta JambieresMeta = (LeatherArmorMeta)leggins.getItemMeta();
		    JambieresMeta.setColor(Color.GREEN);
		    JambieresMeta.spigot().setUnbreakable(true);
		    leggins.setItemMeta(JambieresMeta);
	    ItemStack boots = new ItemStack(Material.matchMaterial("301"), 1);
		    LeatherArmorMeta BottesMeta = (LeatherArmorMeta)boots.getItemMeta();
		    BottesMeta.setColor(Color.GREEN);
		    BottesMeta.spigot().setUnbreakable(true);
		    boots.setItemMeta(BottesMeta);
		ItemStack axe = new ItemStack(Material.ROTTEN_FLESH);
			ItemMeta im = axe.getItemMeta();
			if(Main.getPlugin().getConfig().getBoolean("game.with_entity")) im.addEnchant(new EnchantmentWrapper(16), 2, true);
			else im.addEnchant(new EnchantmentWrapper(16), 1, true);;
			im.spigot().setUnbreakable(true);
			axe.setItemMeta(im);
	
		player.setInvulnerable(true);
		player.getInventory().addItem(axe);
		player.getInventory().setHelmet(head);
		player.getInventory().setChestplate(chestplate);
		player.getInventory().setLeggings(leggins);
		player.getInventory().setBoots(boots);
		player.updateInventory();
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Main.getPlugin(), new Runnable() {
	          public void run() {
	        	  player.setInvulnerable(false);
	          }
		}, 5*20L);
	}
	
	private static void setHumanEquipment(Player player) {
		if(!Main.getPlugin().getConfig().getBoolean("game.with_entity")) player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 0, true));
		ItemStack sword = new ItemStack(Material.WOOD_SWORD);
			ItemMeta im = sword.getItemMeta();
			im.spigot().setUnbreakable(true);
			im.addEnchant(new EnchantmentWrapper(19), 1, true);
			im.addEnchant(new EnchantmentWrapper(20), 1, true);
			sword.setItemMeta(im);
		player.getInventory().addItem(sword);
		 if(APIPlayer.getPlayer(player).hasPermission("infected.gamer")) {
			ItemStack chestplate = new ItemStack(Material.matchMaterial("299"), 1);
			    LeatherArmorMeta PlastronMeta = (LeatherArmorMeta)chestplate.getItemMeta();
			    PlastronMeta.setColor(Color.BLUE);
			    PlastronMeta.spigot().setUnbreakable(true);
			    chestplate.setItemMeta(PlastronMeta);
			player.getInventory().setChestplate(chestplate);
		}
	}
	
	 public static ItemStack addAttributes(ItemStack i){
	        net.minecraft.server.v1_10_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(i);
	        NBTTagCompound compound = nmsStack.getTag();
	        if (compound == null) {
	           compound = new NBTTagCompound();
	            nmsStack.setTag(compound);
	            compound = nmsStack.getTag();
	        }
	        NBTTagList modifiers = new NBTTagList();
	        NBTTagCompound healthboost = new NBTTagCompound();
	        healthboost.set("AttributeName", new NBTTagString("generic.attackDamage"));
	        healthboost.set("Name", new NBTTagString("generic.attackDamage"));
	        healthboost.set("Amount", new NBTTagInt(20));
	        healthboost.set("Operation", new NBTTagInt(0));
	        healthboost.set("UUIDLeast", new NBTTagInt(894654));
	        healthboost.set("UUIDMost", new NBTTagInt(2872));
	        modifiers.add(healthboost);
	        compound.set("AttributeModifiers", modifiers);
	        nmsStack.setTag(compound);
	        i = CraftItemStack.asBukkitCopy(nmsStack);
	        return i;
	    }
	 
	 
	 
}
