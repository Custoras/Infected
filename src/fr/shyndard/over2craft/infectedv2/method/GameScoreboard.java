package fr.shyndard.over2craft.infectedv2.method;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;

import fr.shyndard.over2craft.infectedv2.Main;

@SuppressWarnings("deprecation")
public class GameScoreboard {

	static ScoreboardManager manager;
	static Scoreboard board;
	static Objective objective;
	static Team zombie;
	static Team survivor;
	static Map<String, Integer> data_list = new HashMap<>();
	
	public static void init() {
		manager = Bukkit.getScoreboardManager();
		board = manager.getNewScoreboard();
		zombie = board.registerNewTeam("zombie");
			zombie.setAllowFriendlyFire(false);
			zombie.setPrefix(ChatColor.RED + "");
		survivor = board.registerNewTeam("survivant");
			survivor.setAllowFriendlyFire(false);
			survivor.setPrefix(ChatColor.GREEN + "");
		objective = board.registerNewObjective("infected", "dummy");
			objective.setDisplaySlot(DisplaySlot.SIDEBAR);
			objective.setDisplayName(ChatColor.DARK_GREEN + "Infected");
	}
	
	public static void actualize() {
		data_list.clear();
		if(Main.getPlugin().getConfig().getBoolean("game.with_entity")) {
			data_list.put(ChatColor.LIGHT_PURPLE + "Vague n�" + ChatColor.GOLD + Main.getGame().getWave() + ChatColor.GRAY + " (" + ChatColor.BLUE + Main.getGame().getTimer() + ChatColor.GRAY + ")", 4);
		} else {
			data_list.put(ChatColor.LIGHT_PURPLE + "Arriv�e des secours "  + ChatColor.GOLD + Main.getGame().getTimer(), 4);
		}
		data_list.put(ChatColor.GRAY + "--------------", 3);
		data_list.put(ChatColor.RED + "Infect� " + ChatColor.GRAY + ": " + ChatColor.GOLD + Main.getGame().zombieCount(), 2);
		data_list.put(ChatColor.GREEN + "Survivant " + ChatColor.GRAY + ": " + ChatColor.GOLD + Main.getGame().humanCount(), 1);
		
		for(String value : board.getEntries()) {
			if(data_list.get(value) == null) {
				board.resetScores(value);
			}
		}
		
		for(String value : data_list.keySet()) {
			objective.getScore(value).setScore(data_list.get(value));
		}
	}
	
	public static void setZombie(Player player) {
		survivor.removePlayer(player);
		zombie.addPlayer(player);
	}
	
	public static void setSurvivor(Player player) {
		survivor.addPlayer(player);
	}
	
	public static void set(Player player) {
		player.setScoreboard(board);
	}
	
	
}
