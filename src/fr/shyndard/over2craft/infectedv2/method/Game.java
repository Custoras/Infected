package fr.shyndard.over2craft.infectedv2.method;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import fr.shyndard.over2craft.api.APIGlobalFunction;
import fr.shyndard.over2craft.api.instance.InstanceManagement;
import fr.shyndard.over2craft.api.method.APIActionBar;
import fr.shyndard.over2craft.api.method.CenterMessage;
import fr.shyndard.over2craft.api.method.MapUtil;
import fr.shyndard.over2craft.api.method.Status;
import fr.shyndard.over2craft.api.method.Title;
import fr.shyndard.over2craft.api.player.APIPlayer;
import fr.shyndard.over2craft.api.statistics.APIStatistic;
import fr.shyndard.over2craft.infectedv2.Main;

public class Game {

	GameStatus status;
	Integer timer;
	Integer wave = 0;
	String prefix = ChatColor.GRAY + "[" + ChatColor.DARK_GREEN + "Infected"+ ChatColor.GRAY + "] ";
	Map<Player, Integer> player_role_list = new HashMap<>();
	static List<Integer> zombie_id = new ArrayList<>();
	
	Map<Player, Integer> player_point = new HashMap<>();
	
	//0 : Survivant - 1 : Zombie - 2 : Joueur - 3 : Spectateur//
	
	public Game() {
		this.status = GameStatus.WAITING;
	}
	
	public boolean isHuman(Player player) {
		if(player_role_list.get(player) == null) return false;
		if(player_role_list.get(player) == 0) return true;
		return false;
	}
	
	public boolean isZombie(Player player) {
		if(player_role_list.get(player) == null) return false;
		if(player_role_list.get(player) == 1) return true;
		return false;
	}
	
	public GameStatus getStatus() {
		return this.status;
	}
	
	public String getPrefix() {
		return prefix;
	}
	
	public Integer getTimer() {
		return timer;
	}
	
	public Integer getWave() {
		return wave;
	}
	
	public static List<Integer> getZombieIdList() {
		return zombie_id;
	}
	
	public Map<Player, Integer> getPlayerRoleList() {
		return player_role_list;
	}
	
	public void addPoint(Player player, Integer value, Integer coin) {
		if(value <= 0 && coin <= 0) return;
		if(player_point.get(player) == null) player_point.put(player, value);
		else player_point.put(player, player_point.get(player)+value);
		APIPlayer.getPlayer(player).addCoin(coin, true);
		if(value>0) new Title("", ChatColor.GOLD + "+" + value, 5, 10, 5).send(player);;
	}
	
	public ChatColor getPlayerChatColor(Player player) {
		if(player_role_list.get(player) == 1) return ChatColor.RED;
		if(player_role_list.get(player) == 0) return ChatColor.GREEN;
		return ChatColor.GRAY;
	}

	public void addPlayer(Player player) {
		player_role_list.put(player, 2);
		player.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(16);
		player.setGameMode(GameMode.ADVENTURE);
		Bukkit.broadcastMessage(APIPlayer.getPlayer(player).getRank().getColor() + player.getName() + ChatColor.GRAY + " rejoint la partie (" + Bukkit.getOnlinePlayers().size() + "/" + Main.getPlugin().getConfig().getInt("game.max_player") + ")");
		player.teleport(Main.location_list.get("lobby"));
	}

	public void addSpectator(Player player) {
		player_role_list.put(player, 3);
		player.setGameMode(GameMode.SPECTATOR);
		if(status.getValue() >= GameStatus.INGAME.getValue()) player.teleport(Main.location_list.get("map_spectator"));
		else player.teleport(Main.location_list.get("lobby"));
		player.sendMessage("Vous rejoignez en tant que spectateur.");
	}
	
	public Integer humanCount() {
		Integer count = 0;
		for(Player players : player_role_list.keySet()) {
			if(player_role_list.get(players) == 0) count = count+1;
		}
		return count;
	}
	
	public Integer zombieCount() {
		Integer count = 0;
		for(Player players : player_role_list.keySet()) {
			if(player_role_list.get(players) == 1) count = count+1;
		}
		return count;
	}

	public void remove(Player player) {
		for (Iterator<Entry<Player, Integer>> it = player_role_list.entrySet().iterator(); it.hasNext();) {
			Map.Entry<Player, Integer> entry = it.next();
			if((Player)entry.getKey() == player) {
				it.remove();
			}
		}
		if(getStatus() == GameStatus.INGAME) {
			if(humanCount() == 0) {
				stepEnd();
			}
			else if(zombieCount() == 0) {
				stepEnd();
			}
		}
	}
	
	public void setZombie(Player player) {
		if(!isHuman(player)) return;
		player_role_list.put(player, 1);
		GameScoreboard.setZombie(player);
		player.setPlayerListName(ChatColor.RED + player.getName());
		APIActionBar.sendInfiniteActionBar(player, ChatColor.GRAY + "Vous �tes un " + ChatColor.RED + "zombie" + ChatColor.GRAY + ".");
	}
	
	public void stepLaunch() {
		if(status == GameStatus.LAUNCHING) return;
		if(Bukkit.getOnlinePlayers().size() < Main.getPlugin().getConfig().getInt("game.min_player")) return;
		status = GameStatus.LAUNCHING;
		timer = 30;
		new BukkitRunnable() {
			@Override
            public void run() {
        		if(timer == 0) {
        			stepStart();
        			cancel();
        			return;
        		}
        		if(timer == 60 || timer == 30 || timer == 20 || timer == 10 || timer <= 5) {
					for(Player players : Bukkit.getOnlinePlayers()) {
						players.playSound(players.getLocation(), Sound.BLOCK_NOTE_PLING, 10F, 1F);
						players.setLevel(timer);
					}
					Bukkit.broadcastMessage(prefix + "Lancement dans " + ChatColor.GOLD + timer + ChatColor.GRAY + " seconde" + (timer > 1 ? "s" : "") + ".");
				} else {
    				for(Player players : Bukkit.getOnlinePlayers()) {
    					players.setLevel(timer);
    				}
				}
        		timer = timer-1;
            }
		}.runTaskTimer(Main.getPlugin(), 0, 20L);
	}
	
	public void stepStart() {
		if(status == GameStatus.INGAME) return;
		if(Bukkit.getOnlinePlayers().size() < Main.getPlugin().getConfig().getInt("game.min_player")) {
			Bukkit.broadcastMessage(prefix + ChatColor.RED + "Il n'y a pas assez de joueur.");
			for(Player players : Bukkit.getOnlinePlayers()) {
				players.playSound(players.getLocation(), Sound.ENTITY_VILLAGER_NO, 10F, 1F);
			}
			status = GameStatus.WAITING;
			return;
		}
		
		for(Entity e : Bukkit.getWorlds().get(0).getEntities()) {
			if(e.getType() != EntityType.PLAYER  && e.getType() != EntityType.ITEM_FRAME) {
				e.remove();
			}
		}
		
		Bukkit.getWorlds().get(0).setWeatherDuration(0);
		Bukkit.getWorlds().get(0).setStorm(false);
		Bukkit.getWorlds().get(0).setThunderDuration(0);
		Bukkit.getWorlds().get(0).setThundering(false);
		
		Main.location_list.get("lobby").getWorld().playSound(Main.location_list.get("lobby"), Sound.BLOCK_NOTE_HAT, 100F, 1F);
		status = GameStatus.INGAME;
		InstanceManagement.setActivedChat(false);
		InstanceManagement.setActivedTab(false);
		InstanceManagement.setInformation(Status.INGAME);
		setRandomPlayerRole();
		for(Player players : Bukkit.getOnlinePlayers()) {
			preparePlayer(players);
			GameScoreboard.set(players);
		}
		GameScoreboard.actualize();
		if(Main.getPlugin().getConfig().getBoolean("game.with_entity")) {
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Main.getPlugin(), new Runnable() {
		          public void run() {
		        	  if(status != GameStatus.INGAME) return;
		        	  nextWave();
		          }
			}, 10*20L);
		} else {
			timer = Main.getPlugin().getConfig().getInt("game.game_timer");
			new BukkitRunnable() {
				@Override
	            public void run() {
					if(getStatus() != GameStatus.INGAME) {
						cancel();
	        			return;
					}
					timer--;
					GameScoreboard.actualize();
					if(timer == 0) {
						stepEnd();
						cancel();
	        			return;
					}
	            }
			}.runTaskTimer(Main.getPlugin(), 0, 20L);
		}
	}

	public void stepEnd() {
		if(status == GameStatus.END) return;
		status = GameStatus.END;
		InstanceManagement.setInformation(Status.END);
		for(Entity e : Bukkit.getWorlds().get(0).getEntities()) {
			if(e.getType() != EntityType.PLAYER  && e.getType() != EntityType.ITEM_FRAME) {
				e.remove();
			}
		}
		APIActionBar.clearList();
		Bukkit.broadcastMessage(APIGlobalFunction.getLine());
		if(humanCount() == 0) {
			Bukkit.broadcastMessage(CenterMessage.message(ChatColor.AQUA + "Victoire des " + ChatColor.RED + "zombies" + ChatColor.AQUA + " !"));
			firework(1);
		}
		else {
			Bukkit.broadcastMessage(CenterMessage.message(ChatColor.AQUA + "Victoire des " + ChatColor.GREEN + "survivants" + ChatColor.AQUA + " !"));
			firework(0);
		}
		viewglobalclassement();
		
		for(Player players : player_role_list.keySet()) {
			players.getInventory().remove(Material.WOOD_AXE);
			players.getInventory().remove(Material.WOOD_SWORD);
			if(player_role_list.get(players) == 0) APIStatistic.add(players, 7);
			if(player_role_list.get(players) == 1) APIStatistic.add(players, 8);
		}
		
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Main.getPlugin(), new Runnable() {
	          public void run() {
	        	  InstanceManagement.displayReward();
	          }    
  	  	}, 20*5);
		
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Main.getPlugin(), new Runnable() {
	          public void run() {
	        	  APIGlobalFunction.teleportAllToHub();
	        	  status = GameStatus.STOP;
	        	  Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Main.getPlugin(), new Runnable() {
	    	          public void run() {
	    	        	  InstanceManagement.stopServer();
	    	          }    
	        	  }, 20*2);
	          }
		}, 20*10);
	}
	
	void viewglobalclassement() {
		Integer count = 1;
		Bukkit.broadcastMessage("");
		player_point = MapUtil.sortByValue(player_point);
		
		for(Player players : player_point.keySet()) {
			if(count==1 && player_point.get(players) == 0) {
				Bukkit.broadcastMessage(CenterMessage.message("Personne dans le top :'("));
				break;
			}
			else if(player_point.get(players) == 0) break;
			
			if(count <= 3) {
				Bukkit.broadcastMessage(CenterMessage.message("" + ChatColor.YELLOW + count + ((count > 1) ? "�me" : "er") + " " + ChatColor.GOLD + players.getName() + ChatColor.GRAY + " : " + ChatColor.RED + player_point.get(players)));
			}
			else {
				players.sendMessage(" ");
				players.sendMessage(CenterMessage.message("" + ChatColor.GRAY + "Vous �tes " + ChatColor.YELLOW + count + ((count > 1) ? "�me" : "er")  + ChatColor.GRAY + " avec " + ChatColor.RED + player_point.get(players)));
			}
			count++;
		}
		Bukkit.broadcastMessage(APIGlobalFunction.getLine());
	}
	
	
	private void setRandomPlayerRole() {
		Random randomGenerator;
		List<Player> player_list = new ArrayList<>();
		for(Player players : player_role_list.keySet()) {
			if(player_role_list.get(players) == 2) player_list.add(players);
		}
		Integer zombie_count = 1;
		if(Bukkit.getOnlinePlayers().size() >= 7 && Bukkit.getOnlinePlayers().size() < 13) zombie_count = zombie_count+1;
		if(Bukkit.getOnlinePlayers().size() >= 13) zombie_count = zombie_count+2;
		for(Integer i = 0; i<zombie_count; i++) {
			Player zombie;
			do {
				randomGenerator = new Random();
				zombie = player_list.get(randomGenerator.nextInt(player_list.size()));
			}
			while(player_role_list.get(zombie) == 1);
			player_role_list.put(zombie, 1);
			player_list.remove(zombie);
			GameScoreboard.setZombie(zombie);
			zombie.setPlayerListName(ChatColor.RED + zombie.getName());
			zombie.sendMessage(ChatColor.GRAY + "Vous �tes un " + ChatColor.RED + "zombie" + ChatColor.GRAY +" ! Infectez les survivants.");
			APIActionBar.sendInfiniteActionBar(zombie, ChatColor.GRAY + "Vous �tes un " + ChatColor.RED + "zombie" + ChatColor.GRAY + ".");
		}
		for(Player players : player_list) {
			player_role_list.put(players, 0);
			GameScoreboard.setSurvivor(players);
			players.setPlayerListName(ChatColor.GREEN + players.getName());
			players.sendMessage(ChatColor.GRAY + "Vous �tes un " + ChatColor.GREEN + "survivant" + ChatColor.GRAY +" ! Echappez � l'infection.");
			APIActionBar.sendInfiniteActionBar(players, ChatColor.GRAY + "Vous �tes un " + ChatColor.GREEN + "survivant" + ChatColor.GRAY + ".");
		}
	}
	
	
	private void preparePlayer(Player player) {
		if(player_role_list.get(player) == 0) player.teleport(Main.location_list.get("map_human"));
		else if(player_role_list.get(player) == 1) player.teleport(Main.location_list.get("map_zombie"));
		else if(player_role_list.get(player) == 3) player.teleport(Main.location_list.get("map_spectator"));
		EquipPlayer.set(player, player_role_list.get(player));
	}
	
	private void nextWave() {
		if(status != GameStatus.INGAME) {
			return;
		}
		if(wave == Main.getPlugin().getConfig().getInt("game.wave_count")) {
			stepEnd();
			return;
		}
		wave = wave + 1;
		if(wave > Main.getPlugin().getConfig().getInt("game.wave_count")-2) {
			for(Player players : player_role_list.keySet()) {
				if(player_role_list.get(players) == 0) {
					players.addPotionEffect(new PotionEffect(PotionEffectType.GLOWING, 100, 5));
				}
			}
		}
		
		for(Player players : player_role_list.keySet()) {
			if(player_role_list.get(players) == 0) {
				Location spawn_zombie = null;
				for(String loc : Main.location_list.keySet()) {
					if(loc.startsWith("zombie.")) {
						if(spawn_zombie == null || spawn_zombie.distance(players.getLocation()) > Main.location_list.get(loc).distance(players.getLocation())) {
							spawn_zombie = Main.location_list.get(loc);
						}
					}
				}
				if(Main.getPlugin().getConfig().getBoolean("game.hardcore")) {
					for(Integer i = 0; i<wave*2; i++) {
						Zombie zombie = (Zombie)spawn_zombie.getWorld().spawnEntity(spawn_zombie, EntityType.ZOMBIE);
						zombie.setBaby(true);
						zombie.setTarget(players);
						zombie.setHealth(20);
						zombie.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 1));
					}
				} else {
					for(Integer i = 0; i<wave; i++) {
						Zombie zombie = (Zombie)spawn_zombie.getWorld().spawnEntity(spawn_zombie, EntityType.ZOMBIE);
						zombie.setBaby(false);
						zombie.setTarget(players);
						zombie.setHealth(10);
						zombie.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 1));
					}
				}
			}
		}
		
		timer = Main.getPlugin().getConfig().getInt("game.wave_timer");
		new BukkitRunnable() {
			@Override
            public void run() {
        		if(timer == 0 || status != GameStatus.INGAME) {
        			nextWave();
        			cancel();
        			return;
        		}
        		GameScoreboard.actualize();
        		timer = timer-1;
            }
		}.runTaskTimer(Main.getPlugin(), 0, 20L);
		
		Bukkit.broadcastMessage(prefix + ChatColor.RED + "Nouvelle vague...");
	}
	
	void firework(Integer mode) {
		new BukkitRunnable() {
			@Override
            public void run() {
				if(getStatus() == GameStatus.STOP) {
        			cancel();
        			return;
        		}
				for(Player players : player_role_list.keySet()) {
					if(player_role_list.get(players) == mode) {
						Firework fw = (Firework)players.getWorld().spawnEntity(players.getLocation(), EntityType.FIREWORK);
					    FireworkMeta fwm = fw.getFireworkMeta();
					    Random r = new Random();
					    FireworkEffect.Type type = FireworkEffect.Type.BALL_LARGE;
					    Color color = null;
					    if(mode == 0) color = Color.GREEN;
					    if(mode == 1) color = Color.RED;
					    FireworkEffect effect = FireworkEffect.builder().flicker(r.nextBoolean()).withColor(color).with(type).trail(r.nextBoolean()).build();
					    fwm.addEffect(effect);
					    fwm.setPower(1);
					    fw.setFireworkMeta(fwm);
					}
				}
            }
		}.runTaskTimer(Main.getPlugin(), 0, 2*20L);
	}
}
