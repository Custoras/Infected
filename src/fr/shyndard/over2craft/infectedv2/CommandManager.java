package fr.shyndard.over2craft.infectedv2;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.shyndard.over2craft.api.APIGlobalFunction;
import net.md_5.bungee.api.ChatColor;

public class CommandManager implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String arg, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage("La console n'a pas acc�s � cette commande.");
			return false;
		}
		Player player = (Player)sender;
		if(player.isOp()) {
			if(args.length > 0) {
				if(args[0].equalsIgnoreCase("setlocation") || args[0].equalsIgnoreCase("setloc")) {
					if(args.length == 2 && (args[1].equalsIgnoreCase("lobby") 
							|| args[1].equalsIgnoreCase("map_human") 
							|| args[1].equalsIgnoreCase("map_zombie")
							|| args[1].equalsIgnoreCase("map_spectator"))) {
						Main.getPlugin().getConfig().set("game.location." + args[1].toLowerCase() + ".world", player.getLocation().getWorld().getName());
						Main.getPlugin().getConfig().set("game.location." + args[1].toLowerCase() + ".x", player.getLocation().getX());
						Main.getPlugin().getConfig().set("game.location." + args[1].toLowerCase() + ".y", player.getLocation().getY());
						Main.getPlugin().getConfig().set("game.location." + args[1].toLowerCase() + ".z", player.getLocation().getZ());
						Main.getPlugin().getConfig().set("game.location." + args[1].toLowerCase() + ".yaw", player.getLocation().getYaw());
						Main.getPlugin().getConfig().set("game.location." + args[1].toLowerCase() + ".pitch", player.getLocation().getPitch());
						Main.getPlugin().saveConfig();
						sender.sendMessage(ChatColor.GREEN + "Position " + args[1].toLowerCase() + " enregistr�e.");
					}
					else {
						sender.sendMessage("/infected setloc <lobby|map_human|map_zombie|map_spectator>");
					}
				}
				else if(args[0].equalsIgnoreCase("addZombieSpawnPoint") || args[0].equalsIgnoreCase("addZSP")) {
					Integer id = 0;
					while(Main.getPlugin().getConfig().get("game.location.zombie."+id) != null) {
						id = id + 1;
					}
					Main.getPlugin().getConfig().set("game.location.zombie." + id + ".world", player.getLocation().getWorld().getName());
					Main.getPlugin().getConfig().set("game.location.zombie." + id + ".x", player.getLocation().getX());
					Main.getPlugin().getConfig().set("game.location.zombie." + id + ".y", player.getLocation().getY());
					Main.getPlugin().getConfig().set("game.location.zombie." + id + ".z", player.getLocation().getZ());
					Main.getPlugin().saveConfig();
					sender.sendMessage(ChatColor.GREEN + "Position zombie " + id + " enregistr�e.");
				}
				else if(args[0].equalsIgnoreCase("setmin") || args[0].equalsIgnoreCase("setmax")) {
					if(args.length == 2) {
						if(APIGlobalFunction.isInteger(args[1])) {
							if(args[0].equalsIgnoreCase("setmin")) {
								Main.getPlugin().getConfig().set("game.min_player", Integer.parseInt(args[1]));
								sender.sendMessage(ChatColor.GREEN + "Minimum de joueur enregistr�.");
							}
							else if(args[0].equalsIgnoreCase("setmax")) {
								Main.getPlugin().getConfig().set("game.max_player", Integer.parseInt(args[1]));
								sender.sendMessage(ChatColor.GREEN + "Maximum de joueur enregistr�.");
							}
							Main.getPlugin().saveConfig();
						}
						else {
							sender.sendMessage(ChatColor.RED + "Vous devez pr�ciser un nombre.");
						}
					}
					else {
						sender.sendMessage("/infected <setmin|setmax> <count>");
					}
				}
				else {
					sender.sendMessage(ChatColor.RED + "Argument inconnu.");
				}
			}
			else {
				sender.sendMessage("/infected <setloc|setmin|setmax>");
			}
		}
		else {
			sender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission.");
		}
		return false;
	}

}
