package fr.shyndard.over2craft.infectedv2.event;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.util.Vector;

import fr.shyndard.over2craft.api.method.PlayerDamager;
import fr.shyndard.over2craft.api.player.APIPlayer;
import fr.shyndard.over2craft.api.statistics.APIStatistic;
import fr.shyndard.over2craft.infectedv2.Main;
import fr.shyndard.over2craft.infectedv2.method.GameStatus;
import net.md_5.bungee.api.ChatColor;

public class PlayerDamage implements Listener {
	
	@EventHandler
	public void onDamage(EntityDamageEvent event) {
		if(event.getEntity().getType() != EntityType.PLAYER) {
			return;
		}
		if(Main.getGame().getStatus() != GameStatus.INGAME) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onDamageByEntity(EntityDamageByEntityEvent event) {
		if(event.getDamager().getType() != EntityType.PLAYER) return;
		if(Main.getGame().isZombie((Player)event.getDamager())) {
			if(event.getEntity().getType() == EntityType.ZOMBIE) {
				event.setCancelled(true);
				event.getDamager().sendMessage(ChatColor.GREEN + "Copain !");
			}
		}
		if(event.getEntity().getType() != EntityType.PLAYER) return;
		if(Main.getGame().getStatus() != GameStatus.INGAME) {
			event.setCancelled(true);
			return;
		}
		Player damaged = (Player)event.getEntity();
		Player damager = (Player)event.getDamager();
		if((Main.getGame().isHuman(damaged) && Main.getGame().isHuman(damager))
				|| (Main.getGame().isZombie(damaged) && Main.getGame().isZombie(damager))) {
			event.setCancelled(true);
			return;
		}
		APIPlayer.getPlayer(damaged).getPlayerDamager().addDamager(damager);
	}
	
	@EventHandler
	public void onEntityDeath(EntityDeathEvent event) {
		if(event.getEntityType() == EntityType.ZOMBIE) {
			if(event.getEntity().getKiller() == null || event.getEntity().getKiller().getType() != EntityType.PLAYER) return;
			Main.getGame().addPoint((Player)event.getEntity().getKiller(), 1, 0);
			APIStatistic.add((Player)event.getEntity().getKiller(), 10);
		}
		event.setDroppedExp(0);
		event.getDrops().clear();
	}
	
	@EventHandler
	public void onDeath(PlayerDeathEvent event) {
		event.setDeathMessage(null);
		if(Main.getGame().getStatus().getValue() != GameStatus.INGAME.getValue()) return;
		Player player = event.getEntity();
		event.setDroppedExp(0);
		event.getDrops().clear();
		player.setVelocity(new Vector(0,0,0));
		player.setFireTicks(0);
		try {
			PlayerDamager damager_list = APIPlayer.getPlayer(player).getPlayerDamager();
			if(Main.getGame().isHuman(player)) {
				if(damager_list.getDamagers().size() == 0) {
					Bukkit.broadcastMessage(Main.getGame().getPlayerChatColor(player) + player.getName() + ChatColor.GRAY  + " a �t� infect�.");
				}
				else {
					Player killer = damager_list.getDamagers().get(0);
					for(int i = 0; i<damager_list.getDamagers().size(); i++) {
						if(i == 0) Main.getGame().addPoint(damager_list.getDamagers().get(i), 2, 4);
						else Main.getGame().addPoint(damager_list.getDamagers().get(i), 1, 1);
					}
					APIStatistic.add(killer, 11);
					Bukkit.broadcastMessage(Main.getGame().getPlayerChatColor(player) + player.getName() + ChatColor.GRAY + " a �t� infect� par " + Main.getGame().getPlayerChatColor(killer) + killer.getName() + ".");
				}
			}
			else if(Main.getGame().isZombie(player)) {
				player.getWorld().playSound(player.getLocation(), Sound.ENTITY_ZOMBIE_DEATH, 10F, 1F);
				if(damager_list.getDamagers().size() > 0) {
					for(int i = 0; i<damager_list.getDamagers().size(); i++) {
						if(i == 0) Main.getGame().addPoint(damager_list.getDamagers().get(i), 3, 3);
						else Main.getGame().addPoint(damager_list.getDamagers().get(i), 2, 2);
					}
					APIStatistic.add(damager_list.getDamagers().get(0), 9);
				}
			}
		} catch(Exception ex) {}
		Main.getGame().setZombie(player);
		if(Main.getGame().humanCount() == 0) Main.getGame().stepEnd();
		
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Main.getPlugin(), new Runnable() {
	          public void run() {
	        	  player.spigot().respawn();
	          }
		}, 2L);
	}
}
