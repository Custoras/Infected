package fr.shyndard.over2craft.infectedv2.event;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import fr.shyndard.over2craft.infectedv2.Main;
import fr.shyndard.over2craft.infectedv2.method.GameStatus;
import net.md_5.bungee.api.ChatColor;

public class PlayerChat implements Listener {

	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		if(Main.getGame().getStatus().getValue() >= GameStatus.INGAME.getValue()) {
			for(Player players : Main.getGame().getPlayerRoleList().keySet()) {
				if(Main.getGame().getPlayerRoleList().get(player) == Main.getGame().getPlayerRoleList().get(players)) {
					players.sendMessage(Main.getGame().getPlayerChatColor(player) + player.getName() + ChatColor.GRAY + " > " + event.getMessage());
				}
			}
			event.setCancelled(true);
		}
		else if(Main.getGame().getStatus().getValue() >= GameStatus.END.getValue()) {
			Bukkit.broadcastMessage(Main.getGame().getPlayerChatColor(player) + player.getName() + ChatColor.GRAY + " > " + event.getMessage());
		}
	}
}
