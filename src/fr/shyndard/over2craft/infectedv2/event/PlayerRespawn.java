package fr.shyndard.over2craft.infectedv2.event;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

import fr.shyndard.over2craft.infectedv2.Main;
import fr.shyndard.over2craft.infectedv2.method.EquipPlayer;
import fr.shyndard.over2craft.infectedv2.method.GameStatus;

public class PlayerRespawn implements Listener {
	
	@EventHandler
	public void onRespawn(PlayerRespawnEvent event) {
		if(Main.getGame().getStatus().getValue() <= GameStatus.LAUNCHING.getValue()) {
			event.setRespawnLocation(Main.location_list.get("lobby"));
		}
		else {
			if(Main.getGame().isHuman(event.getPlayer())) {
				event.setRespawnLocation(Main.location_list.get("map_human"));
			}
			else if(Main.getGame().isZombie(event.getPlayer())) {
				event.setRespawnLocation(Main.location_list.get("map_zombie"));
				Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Main.getPlugin(), new Runnable() {
			          public void run() {
			        	  EquipPlayer.set(event.getPlayer(), 1);
			          }
				}, 2L);
			}
			else {
				event.setRespawnLocation(Main.location_list.get("map_spectator"));
			}
		}
	}

}
