package fr.shyndard.over2craft.infectedv2.event;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;

import fr.shyndard.over2craft.api.APIGlobalFunction;
import fr.shyndard.over2craft.api.player.APIPlayer;
import fr.shyndard.over2craft.infectedv2.Main;
import fr.shyndard.over2craft.infectedv2.method.GameStatus;

public class PlayerConnection implements Listener {
	
	@EventHandler
	public void onLogin(PlayerLoginEvent event) {
		if(Bukkit.getOnlinePlayers().size() >= Main.getPlugin().getConfig().getInt("game.max_player")) {
			event.setResult(Result.KICK_FULL);
			event.setKickMessage("Cette partie est pleine.");
		}
		if(Main.getGame().getStatus().getValue() >= GameStatus.INGAME.getValue()) {
			if(APIPlayer.getPlayer(event.getPlayer()).isStaff()) {
				event.setResult(Result.ALLOWED);
			} else {
				event.setResult(Result.KICK_OTHER);
				event.setKickMessage("Cette partie a d�j� commenc�e.");
			}
		}
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		if(Main.getGame().getStatus().getValue() > GameStatus.LAUNCHING.getValue()
				|| Bukkit.getOnlinePlayers().size() >= Main.getPlugin().getConfig().getInt("game.max_player")) {
			Main.getGame().addSpectator(event.getPlayer());
		}
		else {
			Main.getGame().addPlayer(event.getPlayer());
			Main.getGame().stepLaunch();
		}
		APIGlobalFunction.clear(event.getPlayer());
		event.setJoinMessage(null);
	}
	
	@EventHandler
	public void onLeave(PlayerQuitEvent event) {
		Main.getGame().remove(event.getPlayer());
		event.setQuitMessage(null);
	}
}
