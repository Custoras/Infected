package fr.shyndard.over2craft.infectedv2.event;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerDropItemEvent;

import fr.shyndard.over2craft.infectedv2.Main;

public class OtherEvent implements Listener {
	
	@EventHandler
	public void onDropItem(PlayerDropItemEvent event) {
		event.setCancelled(true);
	}

	@EventHandler
	public void onLoseFood(FoodLevelChangeEvent event) {
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onEntitySpawn(EntitySpawnEvent event) {
		if(event.getEntityType() == EntityType.PLAYER) return;
		if(event.getEntityType() != EntityType.ZOMBIE) {
			event.getEntity().remove();
			return;
		}
	}
	
	@EventHandler
	public void onTarget(EntityTargetEvent event) {
		if(event.getEntityType() != EntityType.ZOMBIE) return;
		if(event.getTarget() == null || event.getTarget().getType() != EntityType.PLAYER) return;
		if(Main.getGame().isZombie((Player)event.getTarget())){
			event.setTarget(null);
			event.setCancelled(true);
		}
	}
}
