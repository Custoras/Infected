package fr.shyndard.over2craft.infectedv2;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.plugin.java.JavaPlugin;

import fr.shyndard.over2craft.api.instance.InstanceManagement;
import fr.shyndard.over2craft.api.method.Status;
import fr.shyndard.over2craft.infectedv2.event.OtherEvent;
import fr.shyndard.over2craft.infectedv2.event.PlayerChat;
import fr.shyndard.over2craft.infectedv2.event.PlayerConnection;
import fr.shyndard.over2craft.infectedv2.event.PlayerDamage;
import fr.shyndard.over2craft.infectedv2.event.PlayerInteract;
import fr.shyndard.over2craft.infectedv2.event.PlayerRespawn;
import fr.shyndard.over2craft.infectedv2.method.Game;
import fr.shyndard.over2craft.infectedv2.method.GameScoreboard;

public class Main extends JavaPlugin {

	static Main plugin;
	static Game game;
	public static Map<String, Location> location_list = new HashMap<>();
	
	public void onEnable() {
		plugin = this;	
		game = new Game();
		
		getConfig().options().copyDefaults(true);
        saveDefaultConfig();
        
        loadConfig();
        GameScoreboard.init();
        
        Bukkit.getPluginManager().registerEvents(new PlayerConnection(), this);
		Bukkit.getPluginManager().registerEvents(new PlayerDamage(), this);
		Bukkit.getPluginManager().registerEvents(new PlayerChat(), this);
		Bukkit.getPluginManager().registerEvents(new OtherEvent(), this);
		Bukkit.getPluginManager().registerEvents(new PlayerInteract(), this);
		Bukkit.getPluginManager().registerEvents(new PlayerRespawn(), this);
		
        getCommand("infected").setExecutor(new CommandManager());
        
        InstanceManagement.setInformation(Status.WAITING, getConfig().getInt("game.max_player"));
	}
	
	public static Main getPlugin() {
		return plugin;
	}
	
	public static Game getGame() {
		return game;
	}
	
	private void loadConfig() {		
		try {			
			World human_world = Bukkit.getWorld(getConfig().getString("game.location.map_human.world"));
			Double human_x = getConfig().getDouble("game.location.map_human.x");
			Double human_y = getConfig().getDouble("game.location.map_human.y");
			Double human_z = getConfig().getDouble("game.location.map_human.z");
			Integer human_yaw = getConfig().getInt("game.location.map_human.yaw");
			Integer human_pitch = getConfig().getInt("game.location.map_human.pitch");
			location_list.put("map_human", new Location(human_world, human_x, human_y, human_z, human_yaw, human_pitch));
			
			World zombie_world = Bukkit.getWorld(getConfig().getString("game.location.map_zombie.world"));
			Double zombie_x = getConfig().getDouble("game.location.map_zombie.x");
			Double zombie_y = getConfig().getDouble("game.location.map_zombie.y");
			Double zombie_z = getConfig().getDouble("game.location.map_zombie.z");
			Integer zombie_yaw = getConfig().getInt("game.location.map_zombie.yaw");
			Integer zombie_pitch = getConfig().getInt("game.location.map_zombie.pitch");
			location_list.put("map_zombie", new Location(zombie_world, zombie_x, zombie_y, zombie_z, zombie_yaw, zombie_pitch));
	
			World spectator_world = Bukkit.getWorld(getConfig().getString("game.location.map_spectator.world"));
			Double spectator_x = getConfig().getDouble("game.location.map_spectator.x");
			Double spectator_y = getConfig().getDouble("game.location.map_spectator.y");
			Double spectator_z = getConfig().getDouble("game.location.map_spectator.z");
			Integer spectator_yaw = getConfig().getInt("game.location.map_spectator.yaw");
			Integer spectator_pitch = getConfig().getInt("game.location.map_spectator.pitch");
			location_list.put("map_spectator", new Location(spectator_world, spectator_x, spectator_y, spectator_z, spectator_yaw, spectator_pitch));
			
			World lobby_world = Bukkit.getWorld(getConfig().getString("game.location.lobby.world"));
			Double lobby_x = getConfig().getDouble("game.location.lobby.x");
			Double lobby_y = getConfig().getDouble("game.location.lobby.y");
			Double lobby_z = getConfig().getDouble("game.location.lobby.z");
			Integer lobby_yaw = getConfig().getInt("game.location.lobby.yaw");
			Integer lobby_pitch = getConfig().getInt("game.location.lobby.pitch");
			location_list.put("lobby", new Location(lobby_world, lobby_x, lobby_y, lobby_z, lobby_yaw, lobby_pitch));
			
			for(int id = 0; getConfig().get("game.location.zombie."+id) != null; id++) {
				World zombies_world = Bukkit.getWorld(getConfig().getString("game.location.zombie."+id+".world"));
				Double zombies_x = getConfig().getDouble("game.location.zombie."+id+".x");
				Double zombies_y = getConfig().getDouble("game.location.zombie."+id+".y");
				Double zombies_z = getConfig().getDouble("game.location.zombie."+id+".z");
				location_list.put("zombie."+id, new Location(zombies_world, zombies_x, zombies_y, zombies_z));
			}
			
			human_world.setGameRuleValue("doDaylightCycle", "false");
			human_world.setGameRuleValue("doMobSpawning", "true");
			human_world.setTime(18000);
		}
		catch(Exception ex) {
			System.out.println("Erreur lors du chargement de la configuration");
			System.out.println(ex.getMessage());
			System.out.println(ex.getCause());
		}
	}
}
